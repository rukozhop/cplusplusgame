#include <conio.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <chrono>
#include <thread>
#include <Windows.h>
#include "LevelMap.h"
#include "MainMap.h"
#include <future>

using namespace std;

const int REFRESH_RATE = 100;

string map1 = "level1.txt";
string map2 = "level2.txt";
string map3 = "level3.txt";
string map4 = "level4.txt";

int playingStage = 0;
bool timerLoop = true;

void MainMenu();
void Game();
void Options();
void LevelTimer(LevelMap*);

int main()
{
    MainMenu();
    return 0;
}

void LevelTimer(LevelMap* level) {
    int n = 0;
    while (timerLoop) {
        level->UpdatePhysics();
        n++;
        this_thread::sleep_for(chrono::milliseconds(REFRESH_RATE));
    }
}

void MainMenu() {
    while (true) {
        system("CLS");
        cout << "=======================================" << endl;
        cout << "||                                   ||" << endl;
        cout << "||         1 - Play game             ||" << endl;
        cout << "||         2 - Options               ||" << endl;
        cout << "||         3 - exit                  ||" << endl;
        cout << "||                                   ||" << endl;
        cout << "=======================================" << endl;
        int num;
        num = (int)_getch();
        if (num == 49) Game();
        else if (num == 50) Options();
        else if (num == 51) exit(0);
    }
}

void Options() {
    system("CLS");
    cout << "=======================================" << endl;
    cout << "||   Choose Lvl you want to change   ||" << endl;
    cout << "||          1-Lvl 1 map              ||" << endl;
    cout << "||          2-Lvl 2 map              ||" << endl;
    cout << "||          3-Lvl 3 map              ||" << endl;
    cout << "||          4-Lvl 4 map              ||" << endl;
    cout << "=======================================" << endl;
    cout << "For return to main manu click ESC      " << endl;
    string path;
    int nmb;
    nmb = (int)_getch();
    if (nmb == '1') {
        cout << "Enter name of the map" << endl;
        cin >> path;
        map1 = path;
        MainMenu();
    }
    else if (nmb == '2') {
        cout << "Enter name of the map" << endl;
        cin >> path;
        map2 = path;
        MainMenu();
    }
    else if (nmb == '3') {
        cout << "Enter name of the map" << endl;
        cin >> path;
        map3 = path;
        MainMenu();
    }
    else if (nmb == '4') {
        cout << "Enter name of the map" << endl;
        cin >> path;
        map4 = path;
    }
}

void Game() {
    while (playingStage != -1) {
        if (playingStage == 0) {
            MainMap* worldPtr = nullptr;
            MainMap world(100, "worldMap.txt");
            world.InitializeLevel();
            world.AddLevelObject(5, 4, '@');
            while (true) {
                world.Keyboard();
                if (playingStage != 0) break;
            }
        }

        else if (playingStage > 0) {
            string mapPath;
            if (playingStage == 1) mapPath = map1;
            else if (playingStage == 2) mapPath = map2;
            else if (playingStage == 3) mapPath = map3;
            else if (playingStage == 4) mapPath = map4;
            else {
                cout << "Playing stage error, please restart the game.";
                exit(-1);
            }

            int currentStage = playingStage;
            LevelMap level1(300, 100, mapPath);
            level1.InitializeLevel();
            level1.AddLevelObject(31, 15, '@');
            LevelMap* levelPtr = &level1;
            timerLoop = true;
            auto future = async(LevelTimer, levelPtr);


            while (true) {
                level1.Keyboard();
                if (playingStage != currentStage) {
                    timerLoop = false;
                    break;
                }
            }
        }
    }

    system("cls");
    exit(0);
}