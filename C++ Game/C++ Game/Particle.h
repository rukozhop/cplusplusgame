#pragma once
#include <iostream>
#include <Windows.h>

using namespace std;

class Particle
{
private:
	int pos_x;
	int pos_y;
	bool isActive;
	//short direction;

public:
	Particle();
	Particle(int x, int y);

	void SetLocation(int x, int y);
	void SetLocation(COORD pos);

	COORD GetLocation();
	int GetX();
	int GetY();
	bool GetActive();

	void Advance();

	void Destroy();
};

