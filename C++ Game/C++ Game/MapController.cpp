#include "MapController.h"

MapController::MapController() {
	player_x = 0;
	player_y = 0;
	x_max = 0;
	y_max = 0;
}

void MapController::GoToXY(int x, int y) {
	COORD coord;
	coord.X = x;
	coord.Y = y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

void MapController::CursorVisible(bool visibleFlag) {
	HANDLE out = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_CURSOR_INFO cursorInfo;

	GetConsoleCursorInfo(out, &cursorInfo);
	cursorInfo.bVisible = visibleFlag;
	SetConsoleCursorInfo(out, &cursorInfo);
}

void MapController::Keyboard()
{
	char key;
	int num_key;

	key = _getch();
	num_key = static_cast<int>(key);

	switch (num_key)
	{
	case 119:
		if (CheckTerrain(player_x, player_y - 1) == true) {
			MoveMapElement(player_x, player_y, player_x, player_y - 1); // w
			player_y--;
		}
		break;
	case 97:
		if (CheckTerrain(player_x - 1, player_y) == true) {
			MoveMapElement(player_x, player_y, player_x - 1, player_y);	// a
			player_x--;
		}
		break;
	case 115:
		if (CheckTerrain(player_x, player_y + 1) == true) {
			MoveMapElement(player_x, player_y, player_x, player_y + 1);	// s
			player_y++;
		}
		break;
	case 100:
		if (CheckTerrain(player_x + 1, player_y) == true) {
			MoveMapElement(player_x, player_y, player_x + 1, player_y);	// d
			player_x++;
		}
		break;

	case 27:
		Stop();
		break;

	case 32:
		Action();
		break;

	case 102:
		Confirm();
		break;
	}
}

bool MapController::CheckTerrain(int x, int y) {
	if (levelObjects[y][x] == ' ' || levelObjects[y][x] == 'b') return true;
	else return false;
}

void MapController::LoadToArray(string path) {
	mapFile.open(path);
	if (mapFile.fail()) cout << "Error loading map file. Please check map file path and location." << endl;
	int x = 0;
	int y = 0;
	char symbol;
	while (!mapFile.eof()) {
		mapFile.get(symbol);
		levelObjects[y][x] = symbol;
		x++;
		if (symbol == '\n') {
			y++;
			x = 0;
		}
	}

	x_max = x;
	y_max = y + 1;

	for (int i = 0; i < 20; i++) {
		for (int j = 0; j < 80; j++) {
			levelPattern[i][j] = levelObjects[i][j];
		}
	}
}

void MapController::UpdateAtPosition(int x, int y, char symbol) {
	GoToXY(x, y);
	if (symbol == char(98)) cout << char(254);
	else if (symbol == char(90)) cout << char(178);
	else if (symbol == char(80)) cout << char(177);
	else if (symbol == char(89)) cout << char(176);
	else if (symbol == char(88)) cout << char(219);
	else cout << symbol;
}

void MapController::DrawLevelMap() {
	system("CLS");
	for (int i = 0; i < y_max; i++) {
		for (int j = 0; j < x_max; j++) {
			if (levelObjects[i][j] == char(88)) cout << char(219);
			else if (levelObjects[i][j] == char(90)) cout << char(178);
			else if (levelObjects[i][j] == char(80)) cout << char(177);
			else if (levelObjects[i][j] == char(89)) cout << char(176);
			else if (levelObjects[i][j] == char(98)) cout << char(254);
			else if (levelObjects[i][j] == ' ') cout << " ";
			else if (levelObjects[i][j] == '\n') cout << endl;
			else if (levelObjects[i][j] == '@') cout << "@";
		}
	}
}

void MapController::MoveMapElement(int oldX, int oldY, int newX, int newY) {
	char temp = levelObjects[oldY][oldX];
	levelObjects[newY][newX] = temp;
	levelObjects[oldY][oldX] = levelPattern[oldY][oldX];

	UpdateAtPosition(oldX, oldY, levelObjects[oldY][oldX]);
	UpdateAtPosition(newX, newY, levelObjects[newY][newX]);
}

void MapController::AddLevelObject(int x, int y, char symbol) {
	levelObjects[y][x] = symbol;
	UpdateAtPosition(x, y, symbol);
}

void MapController::TransformLevelObject(int x, int y, char symbol, bool overridePattern) {
	levelObjects[y][x] = symbol;
	if (overridePattern) levelPattern[y][x] = symbol;
	UpdateAtPosition(x, y, symbol);
}

void MapController::DestroyLevelObject(int x, int y, bool overridePattern) {
	levelObjects[y][x] = ' ';
	if (overridePattern) levelPattern[y][x] = ' ';
	UpdateAtPosition(x, y, ' ');
}

char MapController::GetObjectAtPosition(int x, int y) { return levelObjects[y][x]; }

COORD MapController::GetPlayerPosition() {
	COORD pos;
	pos.X = player_x;
	pos.Y = player_y;
	return pos;
}

char MapController::GetPlayerTile() { return levelPattern[player_y][player_x]; }

void MapController::SetPlayerPosition(int x, int y) {
	DestroyLevelObject(player_x, player_y, false);
	player_x = x;
	player_y = y;
	AddLevelObject(player_x, player_y, PLAYER_SYMBOL);
}

void MapController::SetPlayerPosition(COORD pos) {
	DestroyLevelObject(player_x, player_y, false);
	player_x = pos.X;
	player_y = pos.Y;
	AddLevelObject(player_x, player_y, PLAYER_SYMBOL);
}