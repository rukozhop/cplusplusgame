#pragma once
#include <iostream>
#include <iomanip>
#include "MapController.h"

extern int playingStage;

const int INITIAL_GEMS = 0;
const int INITIAL_DAMAGE = 1;
const int INITIAL_PLAYER_HEALTH = 100;

const int BASE1_X = 8;
const int BASE2_X = 19;
const int BASE3_X = 44;
const int BASE4_X = 60;

class MainMap : public MapController
{
private:
	int gems;
	int health;
    int damage;
    bool base1Selected, base2Selected, base3Selected, base4Selected;
    string mapLocation;
public:
    MainMap();
    MainMap(int specifiedGems, string mapPath);

    void SetGems(int specifiedGems);
    void SetHealth(int specifiedHealth);

    int GetGems();
    int GetHealth();

    void InitializeLevel();
    void InformationBar();

    void Action() override;
    void Stop() override;
    void Confirm() override;

    void BaseInfo(int base_x);
    void Shop();
};

