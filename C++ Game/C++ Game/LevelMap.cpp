#include "LevelMap.h"

LevelMap::LevelMap() {
	time = DEFAULT_TIME;
	gems = DEFAULT_GEMS;
	isCompleted = false;
}
LevelMap::LevelMap(int specifiedTime, string mapPath) {
	time = specifiedTime;
	gems = DEFAULT_GEMS;
	isCompleted = false;
	mapLocation = mapPath;
}
LevelMap::LevelMap(int specifiedTime, int specifiedGems, string mapPath) {
	time = specifiedTime;
	gems = specifiedGems;
	isCompleted = false;
	mapLocation = mapPath;
	player_x = INITIAL_PLAYER_X;
	player_y = INITIAL_PLAYER_Y;
}

int LevelMap::GetGems() { return gems; }
int LevelMap::GetTime() { return time; }
bool LevelMap::GetCompleted() { return isCompleted; }
void LevelMap::AddTime(int seconds) { time += seconds; }
void LevelMap::AddGems(int value) { gems += value; }
void LevelMap::SetTime(int seconds) { time = seconds; }
void LevelMap::SetGems(int value) { gems = value; }

void LevelMap::InitializeLevel() {
	LoadToArray(mapLocation);
	DrawLevelMap();
	StatusBar();
	CursorVisible(false);
}

void LevelMap::StatusBar() {
	cout << endl << "Gems: " << gems;
	for (int i = 0; i < STATUSBAR_SPACING; i++) cout << " ";
	cout << "Timer: " << time << endl;
}

void LevelMap::UpdatePhysics() {
	if (particles.size() > 0) {
		for (unsigned int i = 0; i < particles.size(); i++) {
			if (particles[i].GetActive()) {
				if (levelObjects[particles[i].GetY() - 1][particles[i].GetX()] == ' ') {
					MoveMapElement(particles[i].GetX(), particles[i].GetY(), particles[i].GetX(), particles[i].GetY() - 1);
					particles[i].Advance();
				}
				else {
					if (levelObjects[particles[i].GetY() - 1][particles[i].GetX()] == 'Z') {
						TransformLevelObject(particles[i].GetX(), particles[i].GetY() - 1, 'P', true);
					}
					else if (levelObjects[particles[i].GetY() - 1][particles[i].GetX()] == 'P') {
						TransformLevelObject(particles[i].GetX(), particles[i].GetY() - 1, 'Y', true);
					}
					else if (levelObjects[particles[i].GetY() - 1][particles[i].GetX()] == 'Y') {
						DestroyLevelObject(particles[i].GetX(), particles[i].GetY() - 1, true);
					}
					DestroyLevelObject(particles[i].GetX(), particles[i].GetY(), false);
					particles[i].Destroy();
				}
			}
		}

	}
}

void LevelMap::Action() {
	Shoot();
}

void LevelMap::Stop() {
	playingStage = 0;
}

void LevelMap::Confirm() { }

void LevelMap::Shoot() {
	if (levelObjects[player_y - 1][player_x] == ' ') {
		AddLevelObject(player_x, player_y - 1, '*');
		Particle bullet(player_x, player_y - 1);
		particles.push_back(bullet);
	}
}