#include "Entity.h"

Entity::Entity() {
    health = 0;
}
int Entity::GetHealth() { return health; }
void Entity::SetHealth(int HP) { health = HP; }

void Entity::TakeDamage(int damage) {
    health -= damage;
}