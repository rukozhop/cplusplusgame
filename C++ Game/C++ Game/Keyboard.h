#include <iostream>
#include <stdlib.h>
#include <conio.h>
#include <Windows.h>

using namespace std;

class Keyboard {
private:
    int x = 0;
    int y = 0;
public:

    void Animation();
    Keyboard();
    void Go_Up();
    void Go_Left();
    void Go_Down();
    void Go_Right();


    void Animation()
    {
        while (1)
        {
            for (int i = 0; i <= 7; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    cout << " ";
                }
                cout << ".";
                Sleep(65);
                system("cls");
            }
        }
    }

    void Go_Up()
    {
        y--;
        system("cls");
        for (int i = 0; i < y; i++)
        {
            cout << "\n";
        }
        for (int j = 0; j < x; j++)
        {
            cout << " ";
        }
        cout << ".";
    }

    void Go_Left()
    {
        x--;
        system("cls");
        for (int i = 0; i < y; i++)
        {
            cout << "\n";
        }
        for (int j = 0; j < x; j++)
        {
            cout << " ";
        }
        cout << ".";
    }

    void Go_Down()
    {
        y++;
        system("cls");

        for (int i = 0; i < y; ++i)
        {
            cout << "\n";
        }
        for (int j = 0; j < x; ++j) {
            cout << " ";
        }
        cout << ".";
    }

    void Go_Right()
    {
        x++;
        system("cls");
        for (int i = 0; i < y; i++)
        {
            cout << "\n";
        }
        for (int j = 0; j < x; j++)
        {
            cout << " ";
        }
        cout << ".";
    }

    Keyboard()
    {
        char key;
        int num_key;

        key = getch();
        num_key = static_cast<int>(key);

        switch (num_key)
        {
        case 119:
            Go_Up();
            break;
        case 97:
            Go_Left();
            break;
        case 115:
            Go_Down();
            break;
        case 100:
            Go_Right();
            break;
        }
    }
};