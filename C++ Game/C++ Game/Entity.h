#pragma once
class Entity
{
protected:
    int health;

public:
    Entity();

    int GetHealth();

    void SetHealth(int HP);

    void TakeDamage(int damage);
};

