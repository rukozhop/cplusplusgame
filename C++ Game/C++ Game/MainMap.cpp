#include "MainMap.h"

MainMap::MainMap() {

    gems = INITIAL_GEMS;
    health = INITIAL_PLAYER_HEALTH;
    damage = INITIAL_DAMAGE;
    player_x = 0;
    player_y = 0;
    base1Selected = false;
    base2Selected = false;
    base3Selected = false;
    base4Selected = false;
}

MainMap::MainMap(int specifiedGems, string mapPath) {
    gems = specifiedGems;
    health = INITIAL_PLAYER_HEALTH;
    damage = INITIAL_DAMAGE;
    player_x = 5;
    player_y = 4;
    base1Selected = false;
    base2Selected = false;
    base3Selected = false;
    base4Selected = false;
    mapLocation = mapPath;
}

void MainMap::SetGems(int specifiedGems) { gems = specifiedGems; }
void MainMap::SetHealth(int specifiedHealth) { health = specifiedHealth; }
int MainMap::GetGems() { return gems; }
int MainMap::GetHealth() { return health; }

void MainMap::InitializeLevel()
{
    LoadToArray(mapLocation);
    DrawLevelMap();
    InformationBar();
    CursorVisible(false);
}

void MainMap::InformationBar() {
    cout << endl << "Gems - " << gems << ", " << "Health - " << health << ". Press SPACE to enter shop or view base info." << endl;
}

void MainMap::Action() {
    if (GetPlayerTile() == char(98)) {
        COORD location = GetPlayerPosition();
        BaseInfo(location.X);
    }
    else Shop();
}

void MainMap::Stop() {
    playingStage = -1;
}

void MainMap::Confirm() {
    if (base1Selected) playingStage = 1;
    else if (base2Selected) playingStage = 2;
    else if (base3Selected) playingStage = 3;
    else if (base4Selected) playingStage = 4;
}

void MainMap::BaseInfo(int base_x) {
    GoToXY(0, 20);
    if (base_x == BASE1_X) {
        cout << "BASE 1   Difficulty: 1   Maximum gems: 100   Press F to assault.";
        base1Selected = true;
        base2Selected = false;
        base3Selected = false;
        base4Selected = false;
    }
    else if (base_x == BASE2_X) {
        cout << "BASE 2   Difficulty: 2   Maximum gems: 200   Press F to assault.";
        base1Selected = false;
        base2Selected = true;
        base3Selected = false;
        base4Selected = false;
    }
    else if (base_x == BASE3_X) {
        cout << "BASE 3   Difficulty: 3   Maximum gems: 300   Press F to assault.";
        base1Selected = false;
        base2Selected = false;
        base3Selected = true;
        base4Selected = false;
    }
    else if (base_x == BASE4_X) {
        cout << "BASE 4   Difficulty: 4   Maximum gems: 600   Press F to assault.";
        base1Selected = false;
        base2Selected = false;
        base3Selected = false;
        base4Selected = true;
    }
}

void MainMap::Shop() {
    int sk;
    bool loop = true;
    while (loop) {
        std::system("CLS");
        cout << "=======================================" << endl;
        cout << "   HP=" << setw(3) << health << "    DMG=" << setw(3) << damage << "    GEM=" << setw(3) << gems << setw(6) << "" << endl;
        cout << "                                   " << endl;
        cout << "       What you want to buy?       " << endl;
        cout << "        1 - 1HP (1 GEM)            " << endl;
        cout << "        2 - +1DMG (10 GEM)         " << endl;
        cout << "        3 - EXIT                   " << endl;
        cout << "                                   " << endl;
        cout << "=======================================" << endl;
        cout << endl;
        //cin >> sk;
        sk = _getch();
        switch (sk) {
        case 49:
            if (gems > 0) {
                health++;
                gems--;
            }
            else {
                cout << endl;
                cout << "You don't have enought gems." << endl;
                Sleep(5000);
            }
            break;
        case 50:
            if (gems >= 10) {
                damage++;
                gems -= 10;
            }
            else {
                cout << endl;
                cout << "You don't have enought gems." << endl;
                Sleep(5000);
            }
            break;
        case 51:
            loop = false;
            break;
        default:
            break;
        }
    }

    std::system("cls");
    DrawLevelMap();
    InformationBar();
}