#pragma once

#include <iostream>
#include <windows.h>
#include <fstream>
#include <conio.h>
#include <stdlib.h>

using namespace std;

const char PLAYER_SYMBOL = '@';

class MapController
{
protected:
	char levelObjects[20][80];
	char levelPattern[20][80];
	
	int player_x;
	int player_y;
	ifstream mapFile;
	int x_max;
	int y_max;

public:
	MapController();

	void GoToXY(int x, int y);
	void CursorVisible(bool visibleFlag);
	void Keyboard();

	virtual void Stop() = 0;
	virtual void Action() = 0;
	virtual void Confirm() = 0;

	bool CheckTerrain(int x, int y);
    
	void LoadToArray(string path);
	void UpdateAtPosition(int x, int y, char symbol);
	void DrawLevelMap();

	void MoveMapElement(int oldX, int oldY, int newX, int newY);
	void AddLevelObject(int x, int y, char symbol);
	void TransformLevelObject(int x, int y, char symbol, bool overridePattern);
	void DestroyLevelObject(int x, int y, bool overridePattern);

	char GetObjectAtPosition(int x, int y);
	COORD GetPlayerPosition();
	char GetPlayerTile();

	void SetPlayerPosition(int x, int y);
	void SetPlayerPosition(COORD pos);
};

