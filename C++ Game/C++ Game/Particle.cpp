#include "Particle.h"

Particle::Particle() {
	pos_x = 0;
	pos_y = 0;
	isActive = true;
}

Particle::Particle(int x, int y) {
	pos_x = x;
	pos_y = y;
	isActive = true;
}

void Particle::SetLocation(int x, int y) {
	pos_x = x;
	pos_y = y;
}

void Particle::SetLocation(COORD pos) {
	pos_x = pos.X;
	pos_y = pos.Y;
}

COORD Particle::GetLocation() {
	COORD position;
	position.X = pos_x;
	position.Y = pos_y;
	return position;
}

int Particle::GetX() { return pos_x; }
int Particle::GetY() { return pos_y; }
bool Particle::GetActive() { return isActive; }

void Particle::Advance() {
	pos_y--;
}

void Particle::Destroy() { isActive = false; }