#include <iostream>
#include <vector>
#include "MapController.h"
#include "Particle.h"

using namespace std;

extern int playingStage;

const int INITIAL_PLAYER_X = 31;
const int INITIAL_PLAYER_Y = 15;

const int DEFAULT_TIME = 300;
const int DEFAULT_GEMS = 0;
const int STATUSBAR_SPACING = 45;

class LevelMap : public MapController
{
	
private:
	int time;
	int gems;
	bool isCompleted;
	string mapLocation;
	vector<Particle> particles;
	
public:
	LevelMap();
	LevelMap(int specifiedTime, string mapPath);
	LevelMap(int specifiedTime, int specifiedGems, string mapPath);

	int GetGems();
	int GetTime();
	bool GetCompleted();
	void AddTime(int seconds);
	void AddGems(int value);
	void SetTime(int seconds);
	void SetGems(int value);

	void InitializeLevel();
	void StatusBar();

	void UpdatePhysics();

	void Action() override;
	void Stop() override;
	void Confirm() override;

	void Shoot();
};

